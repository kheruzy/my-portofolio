$(document).ready(function() {
	var navi = document.querySelector(".navi");
	var navLinks = document.querySelector(".nav-links");
    var hamburger = document.querySelector(".hamburger");
    var line1 = document.querySelector(".line1");
    var line2 = document.querySelector(".line2");
	// var navi = $('.navi');
	// var navLinks = $('.navi');
	// var dropdown = $('.navi');

	hamburger.addEventListener("click", () => {
		console.log("hihi")
        navLinks.classList.toggle("open");
        line1.classList.toggle("rotate1");
        line2.classList.toggle("rotate2");
    });

    var parallax = document.querySelector('.parallax');
    parallax.addEventListener("click", () => {
        console.log("hihi")
        navLinks.classList.remove("open");
        line1.classList.remove("rotate1");
        line2.classList.remove("rotate2"); 
    });
    
    var prevScrollpos = parallax.scrollTop;
    console.log(prevScrollpos)
    parallax.onscroll = function() {
        slideInScroll()
        navLinks.classList.remove("open");
        line1.classList.remove("rotate1");
        line2.classList.remove("rotate2");
        var currentScrollPos = parallax.scrollTop;
        if (prevScrollpos > currentScrollPos) {
            document.getElementsByClassName("navbar")[0].style.top = "0";
        } else {
            document.getElementsByClassName("navbar")[0].style.top = "-80px";
        }
        prevScrollpos = currentScrollPos;
    }
});