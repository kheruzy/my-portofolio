$(document).ready(function() {
    console.log(position())
    $('.change1').css('background-position', position())
    $('.change2').css('background-position', position())
    slideInScroll()
})

function position() {
    var position = ''
    for (var i = 0; i < 4; i++) {
        var rand = Math.floor(Math.random() * 111)-5;
        position += rand.toString() + "% "
        if (i % 2 != 0) {
            position += ', '
        }
    }
    return position
}

function slideInScroll() {
    var section = $('.section')
    section.each(function(i, el){
        var el = $(el)
        var frames = el.find('.frame')
        if (el.visible(true)) {
            $.each(frames, function(i, frame) {
                var frame = frames[i]
                setTimeout(function() {
                    frames.find('.timeline_pc div').addClass('timeline_show')
                    frames.find('.timeline_mobile div').addClass('timeline_show')
                    frame.classList.add('appear')
                }, i*100)
            })
        }
    })
}