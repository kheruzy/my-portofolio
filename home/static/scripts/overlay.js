function img_list_f(img_list) {
    var pics = ''
    console.log(img_list)
    for (idx in img_list) {
        pics += `<img src="../static/images/logos_hover/${img_list[idx]}" alt="">`
    }
    return pics
}

function overlayOn(id) {
    var title = ""
    var year = ""
    var content = ""
    var img_list = []
    switch (id) {
        case "hubble":
            title = 'Hubble Aeronautics'
            year = '2018'
            content = "Hubble is an imaginary company specializing in aeronautics equipment. This logo consists of an airplane and ‘H’ for the company’s abbreviation in the logo’s negative spaces. The airplane shown is also disassembled, communicating its specialization."
            img_list = [
                'hubble.png',
            ]
            break
        case "uneev":
            title = 'Uneev'
            year = '2018'
            content = "Uneev was an online learning platform that was intended to specialize in programming languages. The ‘U’ on the logo resembles a small plant, symbolizing growth and early development.  The whole custom logo typography also follows its logo style. Purple is to convey that Uneev promotes creativity, dignity, and wisdom."
            img_list = [
                'uneev.png',
                'uneev1.png',
            ]
            break
        case "bok":
            title = 'BOK Beachfront Cafe'
            year = '2016'
            content = "BOK Beachfront Cafe is a cafe that is located in Bokarina, Australia that serves coffees and sweets. As the name suggests, it is located by a beach. They came to Freelancer.com to open a competition for their logo. Unfortunately, this logo isn’t the chosen one."
            img_list = [
                'bok.png',
            ]
            break
        case "dp":
            title = 'Direct Planet'
            year = '2018'
            content = ""
            img_list = [
                'dp.png',
                'dp1.png'
            ]
            break
        case "bsc":
            title = 'Bhakti Strategic Consulting'
            year = '2018'
            content = "Bhakti Strategic Consulting is a business consulting company specializing in management and training founded in 2009. The simple logo consists of their abbreviation, BSC, and a speech bubble, to convey that they are communicable, hospitable, and trustworthy. It also compliments the blue color that symbolizes trust, loyalty, wisdom, and confidence."
            img_list = [
                'bsc.png',
            ]
            break
        case "fg":
            title = 'ForeGround Logos'
            year = '2018'
            content = "ForeGround Logos was a personal project that was intended to provide a custom logo and typefaces for startups and/or projects. The ‘F’ and ‘G’ from ‘foreground’ is constructed in such a way that the two letters are readable while being in the same shape."
            img_list = [
                'fg.png',
            ]
            break
        case "dobutsu":
            title = 'Dobutsu'
            year = '2019'
            content = "Dobutsu is a fashion/tote bag company showcasing their two Coco the cat and Yuki the shiba inu character. This logo minimalistically displays the two characters in a playful manner. "
            img_list = [
                'dobutsu.png',
            ]
            break
        case "semendo":
            title = 'Cangkir Semendo'
            year = '2018'
            content = "Cangkir Semendo is a coffee shop in Jakarta. The clients wanted to showcase their own planted coffee seeds and the atmosphere the cafe gives. This logo is pretty compact, it logo showcases the warmth, the beans they provide, the "
            img_list = [
                'semendo.png'
            ]
            break
        case "bjc":
            title = 'Sansation'
            year = '2016'
            content = "Sansation (Satu Basketball Invitation) is a basketball competition held by SMAN 1 Jakarta. The color (red, blue, and white) represents the Netherlands flag due to their significant role in the school’s early history. The giant basketball over the silhouette of Jakarta’s skyline represents that this event invites schools from around Jakarta in order to scout and invites the best talent Jakarta has to offer."
            img_list = [
                'bjc.png'
            ]
            break
        case "quanta":
            title = 'Quanta'
            year = '2018'
            content = "Quanta is the name given to the class of 2018 of the Faculty of Computer Science of University Indonesia. The logo combines the name’s initial and the shape of an atom."
            img_list = [
                'quanta.png'
            ]
            break
        case "kamrad":
            title = 'Kamrad Clothing'
            year = '2018'
            content = "Kamrad Clothing is a fashion startup that started in 2018. They design and sell shirts that contain anarchy and rebellious messages. Hence the A symbol."
            img_list = [
                'kamrad.png'
            ]
            break
    }


    $('.overlay_title').html(title)
    $('.year').html(year)
    $('.overlay_para').html(content)
    $('.overlay_pic').html(img_list_f(img_list))

    $('.overlay').addClass("overlay-show")
    $('.overlay_card').addClass("overlay_card-show")
}

function overlayOff() {
    $('.overlay').removeClass("overlay-show")
    $('.overlay_card').removeClass("overlay_card-show")
}