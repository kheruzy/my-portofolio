from django.shortcuts import render, redirect
from collections import namedtuple

# Create your views here.
def index(request):
    return render(request, 'home.html')

def about(request):
    return render(request, 'about.html')

def portofolio(request):
    return render(request, 'portofolio.html')